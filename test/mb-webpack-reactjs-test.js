/* eslint no-unused-expressions:0 */
import {expect} from 'chai';
import WebpackReactjs from '../src/mb-webpack-reactjs';

describe('mb-webpack-reactjs component test suite', () => {

  describe('mb-webpack-reactjs', () => {
    it('should be loaded properly', () => {
      expect(WebpackReactjs).to.not.be.undefined;
    });
  });

});
