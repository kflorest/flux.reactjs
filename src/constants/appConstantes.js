let itens = {
  menu : [
    {
      icon:'fa fa-car',
      smallName: 'A',
      fullName: 'Actions',
      link:'#/detailContent',
      plus: true,
      detail:[
        {
          smallName: 'S',
          fullName: 'Sell',
          link:'#/PageSell'
        },
        {
          smallName: 'B',
          fullName: 'Buy',
          link:'#/buy'
        },
        {
          smallName: 'A',
          fullName: 'Armory',
          link:'#/armory'
        },
        {
          smallName: 'S',
          fullName: 'Shopping',
          link:'#/shooping'
        }
      ]
    },
    {
      icon:'fa fa-bolt',
      smallName: 'C',
      fullName: 'Contacts',
      link:'#/PageContacts',
      plus: false
    },
    {
      icon:'fa fa-child',
      smallName: 'C',
      fullName: 'Constants',
      link:'#/PageConstants',
      plus: false
    },
    {
      icon:'fa fa-coffee',
      smallName: 'D',
      fullName: 'Dispatcher',
      link:'#/detailContent',
      plus: true,
      detail:[
        {
          smallName: 'U',
          fullName: 'User',
          link:'#/user'
        },
        {
          smallName: 'M',
          fullName: 'Member',
          link:'#/member'
        },
        {
          smallName: 'P',
          fullName: 'Profile',
          link:'#/profile'
        }
      ]
    },
    {
      icon:'fa fa-bomb',
      smallName: 'S',
      fullName: 'Store',
      link:'#/PageStore',
      plus: false
    }
  ],
  menuHeader: ['Usuario','Logout']
}

export default{
  menu(){
    return(
      itens.menu
    )
  },
  menuHeader(){
    return(
      itens.menuHeader
    )
  }
};
