import React from 'react';
import classNames from 'classnames';
import MenuDetail from './menu.detail'
export default class MenuItem extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      showDetail : false,
      stateLocation: window.location.hash
    };
    this.detail = this.detail.bind(this);
  }
  detail(){
    this.setState({
      showDetail : !this.state.showDetail
    });
  }
  render(){
    let detailClass = classNames('menu-detail',{'menu-detail-show': this.state.showDetail});
    return(
      <li onClick={this.props.item.plus ? this.detail :null}
        style={this.state.showDetail ? {background: '#FFB300'} : null}>
        <div className="item-description">
          <a href={this.props.item.link}>
          {this.props.item.plus ? <i className="fa fa-plus"></i> : null }
          {this.props.item.fullName}
          </a>
        </div>
        <div className="item-icon"><i className={this.props.item.icon}></i></div>
        {this.props.item.plus ? <MenuDetail classMenu={detailClass} items={this.props.item.detail}/> : null }
      </li>
    );
  }
}
