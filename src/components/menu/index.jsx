import React from 'react';
import MenuItem from './menu.item';
import MenuHeader from './menu.header';
import MenuSearch from './menu.search';
export default class MenuFlux extends React.Component{
  constructor(props){
    super(props)
  }
  listMenuItens(){
    return(
      this.props.items.map(function(item,index){
        return(
          <MenuItem key={index} item={item}/>
        )
      })
    )
  }
  render(){
    return(
      <div className="menu-flux">
        <MenuHeader />
        <MenuSearch />
        <ul>
          {this.listMenuItens()}
        </ul>
      </div>
    );
  }
}
