import React from 'react';

export default class MenuDetail extends React.Component{
  constructor(props){
    super(props)
    console.log(props.classMenu)
  }
  detail(){
    return(
      this.props.items.map(function(item,index) {
        return(
          <li key={index}><a href={item.link}>{item.fullName}</a></li>
        )
      })
    )
  }
  render(){
    return(
      <div className={this.props.classMenu}>
        <ul>
          {this.detail()}
        </ul>
      </div>
    )
  }
}
