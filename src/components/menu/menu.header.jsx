import React from 'react';

export default class MenuHeader extends React.Component{
  constructor(props){
    super(props)
  }
  render(){
    return(
      <div className="menu-header">
        <div><i className="menu-icon fa fa-bars fa-2x"></i></div>
      </div>
    )
  }
}
