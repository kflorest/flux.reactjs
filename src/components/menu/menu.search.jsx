import React from 'react';

export default class MenuSearch extends React.Component{
  constructor(props){
    super(props)
  }
  render(){
    return(
      <div className="menu-search">
        <div>
          <input type="text" className="search-input" required></input>
          <span className="menu-icon fa fa-search fa-2x"></span>
        </div>
      </div>
    )
  }
}
