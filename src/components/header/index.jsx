import React from 'react';

export default class HeaderFlux extends React.Component{
  constructor(props){
    super(props)
  }
  listItems(){
    return(
      this.props.items.map(function(item,index){
        return(
          <li key={index}>{item}</li>
        )
      })
    )
  }
  render(){
    return(
      <div className="header-flux">
        <ul>
          {this.listItems()}
        </ul>
      </div>
    );
  }
}
