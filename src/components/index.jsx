import React from 'react';
import ReactDom from 'react-dom';
import HeaderFlux from './header';
import MenuFlux from './menu';
import BodyFlux from './body';
import MainStore from '../store/mainStore';

export default class WebMain extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      menu : MainStore.getMenu(),
      menuHeader : MainStore.getMenuHeader(),
      location: ''
    };
    this.navigated = this.navigated.bind(this);
  }
  navigated() {
    console.log(window.location.pathname);
    console.log(window.location.search);
    this.setState({location: window.location.hash});
  }
  listener(){
    window.addEventListener('hashchange', this.navigated, false);
  }
  setState(changes){
    var component = React.createElement(BodyFlux,Object.assign({},changes));
    ReactDom.render(component, document.getElementById('bodyFlux'));
  }
  render() {
    {this.listener()}
    return (
      <div className='main'>
        <MenuFlux items={this.state.menu} />
        <div className="body-content" id="bodyFlux"></div>
      </div>
    );
  }
}
