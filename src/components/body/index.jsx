import React from 'react';
import PageConstants from './pages/constants';
import PageContacts from './pages/contacts';
import PageSell from './pages/sell';
import PageStore from './pages/store';
import PageNotFound from './pages/notfound'
export default class BodyFlux extends React.Component{
  constructor(props){
    super(props);
  }
  navigation(){
    var loc = this.props.location.replace(/^#\/?|\/$/g, '');
    var component;
    switch (loc) {
      case "PageContacts":
        console.log(loc);
        component = React.createElement(PageContacts);
        break;
      case "PageConstants":
        component = React.createElement(PageConstants);
        break;
      case "PageSell":
        component = React.createElement(PageSell);
        break;
      case "PageStore":
        component = React.createElement(PageStore);
        break;
      default:
        component = React.createElement(PageNotFound);
    }
    return(
      component
    )
  }
  render(){
    return(
      <div className="body-flux">
        {this.navigation()}
      </div>
    );
  }
}
