import React from 'react';

export default class PageNotFound extends React.Component{
  constructor(props){
    super(props);
  }

  render(){
    return(
      <h1>Not found</h1>
    );
  }
}
