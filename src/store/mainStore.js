import appConstantes from '../constants/appConstantes';
import objectAssign  from 'react/lib/Object.assign';
import eventEmitter from 'events';

let CHANGE_EVENT = 'change';
let _store = {
  menu:[],
  menuHeader:[]
};

export default{
  getMenu(){
    return (
      _store.menu = appConstantes.menu()
    )
  },
  getMenuHeader(){
    return(
      _store.menuHeader = appConstantes.menuHeader()
    )
  }
};
